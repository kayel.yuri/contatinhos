package model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ModelContatinho(
    val name: String,
    val user: String,
    val thumb: String
) : Parcelable