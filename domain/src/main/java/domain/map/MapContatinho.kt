package domain.map

import data.entity.EntityContatinho
import model.ModelContatinho

fun List<EntityContatinho>.entityToModel() = mutableListOf<ModelContatinho>().also { modelList ->
    forEach { entity ->
        modelList.add(
            ModelContatinho(
                name = entity.name ?: "",
                user = entity.username ?: "",
                thumb = entity.img ?: ""
            )
        )
    }
}
