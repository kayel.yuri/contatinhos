package domain.usecases

import arrow.core.Either.Left
import arrow.core.Either.Right
import base.error.Failure
import data.repository.IRepositoryRM
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class UseCaseEpisodes(private val repoContatinho: IRepositoryRM) {

    suspend fun execute(page: Int = 1) = withContext(IO) {
        try {
            Right(repoContatinho.getEpisodes(page))
        } catch (exception: Exception) {
            Left(Failure(exception.message, exception))
        }
    }
}