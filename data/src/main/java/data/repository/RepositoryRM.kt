package data.repository

import data.entity.EntityCharList
import data.entity.EntityEpisodeList
import data.entity.EntityLocationList
import data.source.ISourceRM

class RepositoryRM(private val sourceRM: ISourceRM) : IRepositoryRM {

    override suspend fun getChars(page: Int): EntityCharList = sourceRM.getChars(page)

    override suspend fun getLocations(page: Int): EntityLocationList = sourceRM.getLocations(page)

    override suspend fun getEpisodes(page: Int): EntityEpisodeList = sourceRM.getEpisodes(page)

}