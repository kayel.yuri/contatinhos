package data.repository

import data.entity.EntityCharList
import data.entity.EntityEpisodeList
import data.entity.EntityLocationList

interface IRepositoryRM {

    suspend fun getChars(page: Int = 1): EntityCharList

    suspend fun getLocations(page: Int = 1): EntityLocationList

    suspend fun getEpisodes(page: Int = 1): EntityEpisodeList
}
