package data.source

import data.entity.EntityCharList
import data.entity.EntityEpisodeList
import data.entity.EntityLocationList

interface ISourceRM {

    suspend fun getChars(page: Int = 1): EntityCharList
    suspend fun getLocations(page: Int = 1): EntityLocationList
    suspend fun getEpisodes(page: Int = 1): EntityEpisodeList

}
