package data.source

import base.receiver.IConnectivity
import data.common.RetroCache
import data.common.RetroConnect
import data.common.RetroSource
import data.service.ServiceRM
import java.io.File

private const val baseURL = "https://rickandmortyapi.com/api/"

class SourceRM(connectivity: IConnectivity, cacheDir: File) :
    RetroSource<ServiceRM>(
        RetroConnect(baseURL, connectivity),
        RetroCache(cacheDir)
    ),
    ISourceRM {

    override suspend fun getChars(page: Int) = service.getChars(page)

    override suspend fun getLocations(page: Int) = service.getLocations(page)

    override suspend fun getEpisodes(page: Int) = service.getEpisodes(page)

}