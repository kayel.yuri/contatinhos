package data.service

import data.common.RetroService
import data.entity.EntityCharList
import data.entity.EntityEpisodeList
import data.entity.EntityLocationList
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiceRM : RetroService {

    @GET("character/?")
    suspend fun getChars(
        @Query("page") page: Int = 1
    ): EntityCharList

    @GET("location/?")
    suspend fun getLocations(
        @Query("page") page: Int = 1
    ): EntityLocationList

    @GET("episode/?")
    suspend fun getEpisodes(
        @Query("page") page: Int = 1
    ): EntityEpisodeList
}