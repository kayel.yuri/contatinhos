package data.common

abstract class Source<Service>{
    abstract val service : Service
}
