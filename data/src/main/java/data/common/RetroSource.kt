package data.common

open class RetroSource<Service : RetroService>(
    connect: RetroConnect<Service>,
    cache: RetroCache? = null,
    auth: RetroAuth? = null
) : Source<Service>() {
    override val service: Service = RetroInit(connect, cache, auth).create()
}

