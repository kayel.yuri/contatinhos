package data.common

import java.io.File

class RetroCache(
    val cacheDir : File,
    val minutesMaxAge: Int = 5,
    val hoursMaxStale: Int = 24,
    val size: Long = 5 * 1024 * 1024,
    val path: String = "responses"
)