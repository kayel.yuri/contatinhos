package data.common

import okhttp3.Cache
import okhttp3.CacheControl.Builder
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit.HOURS
import java.util.concurrent.TimeUnit.MINUTES
import java.util.concurrent.TimeUnit.SECONDS

private const val AUTH = "Authorization"
private const val CACHE = "Cache-Control"
private const val PRAGMA = "Pragma"

class RetroInit<Service : RetroService>(
    private val connect: RetroConnect<Service>,
    private val cache: RetroCache? = null,
    private val auth: RetroAuth? = null
) {

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(connect.url)
            .addConverterFactory(converter)
            .client(httpClient)
            .build()
    }

    private val converter by lazy { GsonConverterFactory.create() }

    private val httpClient by lazy {
        OkHttpClient.Builder().apply {
            readTimeout(22, SECONDS)
            connectTimeout(22, SECONDS)
            writeTimeout(22, SECONDS)
            addNetworkInterceptor(networkInterceptor)
            if (auth != null) addInterceptor(authInterceptor)
            if (cache != null) {
                cache(Cache(File(cache.cacheDir, cache.path), cache.size))
                addInterceptor(offlineInterceptor)
            }
        }.build()
    }

    private val networkInterceptor = Interceptor {
        it.proceed(it.request()).newBuilder()
            .removeHeader(PRAGMA)
            .removeHeader(CACHE)
            .header(CACHE, Builder().maxAge(cache?.minutesMaxAge ?: 5, MINUTES).build().toString())
            .build()
    }

    private val authInterceptor = Interceptor {
        var request = it.request()
        if (auth != null && auth.user.isNotEmpty() && auth.pass.isNotEmpty() && connect.connectivity.isOnline) {
            request = request.newBuilder()
                .header(AUTH, Credentials.basic(auth.user, auth.pass))
                .build()
        }
        it.proceed(request)
    }

    private val offlineInterceptor = Interceptor {
        var request = it.request()
        if (connect.connectivity.isOnline) {
            request = request.newBuilder()
                .removeHeader(PRAGMA)
                .removeHeader(CACHE)
                .cacheControl(Builder().maxStale(cache?.hoursMaxStale ?: 1, HOURS).build())
                .build()
        }
        it.proceed(request)
    }

    fun create(): Service = retrofit.create(connect.service.java)
}
