package data.common

import base.receiver.IConnectivity
import kotlin.reflect.KClass

@Suppress("FunctionName")
inline fun <reified Service : RetroService> RetroConnect(
    url: String,
    connectivity: IConnectivity,
): RetroConnect<Service> = RetroConnect(url, connectivity, Service::class)

class RetroConnect<Service : RetroService>(
    val url: String,
    val connectivity: IConnectivity,
    val service: KClass<Service>
)