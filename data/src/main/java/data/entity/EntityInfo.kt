package data.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import org.json.JSONObject

@Parcelize
data class EntityInfo(
    val count: Int = 0,
    val next: String = "",
    val pages: Int = 0,
    val prev: String = ""
) : Parcelable {
    companion object {
        @JvmStatic
        fun buildFromJson(jsonObject: JSONObject?): EntityInfo? {

            jsonObject?.run {
                return EntityInfo(
                    optInt("count"),
                    optString("next"),
                    optInt("pages"),
                    optString("prev")
                )
            }
            return null
        }
    }
}