package data.entity

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class EntityCharList(
    @SerializedName("info")
    val entityInfo: EntityInfo = EntityInfo(),
    @SerializedName("results")
    val characters: MutableSet<Character> = mutableSetOf()
) : Parcelable {

    @SuppressLint("ParcelCreator")
    @Parcelize
    data class Character(
        @SerializedName("created")
        val created: String = "",
        @SerializedName("episode")
        val episode: List<String> = listOf(),
        @SerializedName("gender")
        val gender: String = "",
        @SerializedName("id")
        val id: Int = 0,
        @SerializedName("image")
        val image: String = "",
        @SerializedName("location")
        val charLocation: CharLocation = CharLocation(),
        @SerializedName("name")
        val name: String = "",
        @SerializedName("origin")
        val origin: Origin = Origin(),
        @SerializedName("species")
        val species: String = "",
        @SerializedName("status")
        val status: String = "",
        @SerializedName("type")
        val type: String = "",
        @SerializedName("url")
        val url: String = ""
    ) : Parcelable {
        @SuppressLint("ParcelCreator")
        @Parcelize
        data class CharLocation(
            @SerializedName("name")
            val name: String = "",
            @SerializedName("url")
            val url: String = ""
        ) : Parcelable

        @SuppressLint("ParcelCreator")
        @Parcelize
        data class Origin(
            @SerializedName("name")
            val name: String = "",
            @SerializedName("url")
            val url: String = ""
        ) : Parcelable
    }
}