package data.entity

import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import android.os.Parcelable

@Parcelize
data class EntityEpisodeList(
    @SerializedName("info") val info: EntityInfo = EntityInfo(),
    @SerializedName("results") val episodes: Set<Episode> = setOf()
) : Parcelable {

    @Parcelize
    data class Episode(
        @SerializedName("air_date") val airDate: String = "",
        @SerializedName("characters") val characters: List<String> = listOf(),
        @SerializedName("created") val created: String = "",
        @SerializedName("episode") val episode: String = "",
        @SerializedName("id") val id: Int = 0,
        @SerializedName("name") val name: String = "",
        @SerializedName("url") val url: String = ""
    ) : Parcelable
}