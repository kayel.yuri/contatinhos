package data.entity

import com.google.gson.annotations.SerializedName

data class EntityContatinho(
    @SerializedName("img") val img: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("username") val username: String? = ""
)