package data.entity

import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import android.os.Parcelable

@Parcelize
data class EntityLocationList(
    @SerializedName("info") val info: EntityInfo = EntityInfo(),
    @SerializedName("results") val locations: Set<Location> = setOf()
) : Parcelable {

    @Parcelize
    data class Location(
        @SerializedName("created") val created: String = "",
        @SerializedName("dimension") val dimension: String = "",
        @SerializedName("id") val id: Int = 0,
        @SerializedName("name") val name: String = "",
        @SerializedName("residents") val residents: List<String> = listOf(),
        @SerializedName("type") val type: String = "",
        @SerializedName("url") val url: String = ""
    ) : Parcelable
}