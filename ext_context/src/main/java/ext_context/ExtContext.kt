package ext_context

import android.accessibilityservice.AccessibilityServiceInfo
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.content.ContextWrapper
import android.content.Intent
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager.findFragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import java.io.IOException
import kotlin.reflect.KClass

private const val EMPTY_STRING = ""

val Context.activity: AppCompatActivity
    get() = when (this) {
        is AppCompatActivity -> this
        else -> (this as ContextWrapper).baseContext.activity
    }

val Context.isOnline
    get() =
        (getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager?)?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                it.getNetworkCapabilities(it.activeNetwork)?.run {
                    return@let hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                            hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                            hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                }
            } else {
                @Suppress("DEPRECATION")
                it.activeNetworkInfo?.isConnectedOrConnecting
            }
        } ?: false

fun Context.startActivity(kClass: KClass<*>, extras: Bundle? = null) =
    Intent(this, kClass.java).let {
        if (extras is Bundle) it.putExtras(extras)
        startActivity(it)
    }

fun View.startActivity(kClass: KClass<*>, extras: Bundle? = null) =
    Intent(context, kClass.java).let {
        if (extras is Bundle) it.putExtras(extras)
        context.startActivity(it)
    }

fun Context.stringAny(text: Any?): String = when (text) {
    is String -> text
    is CharSequence -> text.toString()
    is Pair<*, *> -> stringAny(text.first) + " " + stringAny(text.second)
    is Int -> getResourceOrToString(text)
    else -> ""
}

private fun Context.getResourceOrToString(text: Int) = try {
    getString(text)
} catch (ex: Resources.NotFoundException) {
    text.toString()
}

fun Context?.jsonFile(name: String = "mock/pagamentoDetalhe.json"): String = try {
    this?.assets?.open(name)?.run {
        ByteArray(available()).let {
            read(it)
            close()
            String(it)
        }
    } ?: EMPTY_STRING
} catch (ex: IOException) {
    ex.printStackTrace()
    EMPTY_STRING
}

val Context.accessibilityManager: AccessibilityManager
    get() = getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager

fun Context.isAccessibilityEnabled() = accessibilityManager.isEnabled

fun Context.isScreenReaderEnabled(): Boolean {
    if (!accessibilityManager.isEnabled) return false

    val serviceInfoList =
        accessibilityManager.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_SPOKEN)
    if (serviceInfoList.isNullOrEmpty()) return false

    return true
}

fun Context.announceAccessibility(@StringRes stringId: Int) =
    announceAccessibility(getString(stringId))

fun Context.announceAccessibility(stringRes: String) {
    val event = AccessibilityEvent.obtain().apply {
        eventType = AccessibilityEvent.TYPE_ANNOUNCEMENT
        className = javaClass.name
        packageName = packageName
        text.add(stringRes)
    }
    if (isAccessibilityEnabled()) accessibilityManager.sendAccessibilityEvent(event)
}

val Context.inflater get() = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater

fun Context.readAsset(path: String) = assets.open(path).bufferedReader().use { it.readText() }

fun Context.getPathToAssets(folder: String): ArrayList<String> {
    val assetList = arrayListOf<String>()
    assets.list(folder)?.forEach { item ->
        assetList.add("$folder/$item")
    }
    return assetList
}

fun Context.startImageIntent(
    action: String = Intent.ACTION_VIEW,
    uri: Uri?,
    chooserText: String? = ""
) =
    startActivity(
        Intent.createChooser(
            Intent()
                .setAction(action).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                .setDataAndType(uri, "image/*").putExtra(Intent.EXTRA_STREAM, uri), chooserText
        )
    )

fun Context.startHtmlIntent(
    action: String = Intent.ACTION_VIEW,
    uri: Uri?,
    chooserText: String? = ""
) =
    startActivity(
        Intent.createChooser(
            Intent()
                .setAction(action).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                .setDataAndType(uri, "text/html").putExtra(Intent.EXTRA_STREAM, uri), chooserText
        )
    )

val View.viewLifecycleOwner
    get() : LifecycleOwner = try {
        findFragment<Fragment>(this)
    } catch (illegal: IllegalStateException) {
        context.activity
    }

@Throws(java.lang.IllegalStateException::class)
fun View.observeLifecycle() =
    if (this is LifecycleObserver) viewLifecycleOwner.lifecycle.addObserver(this)
    else throw java.lang.IllegalStateException("View $this does not implement LifecycleObserver")