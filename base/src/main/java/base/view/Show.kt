package base.view

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.res.Resources
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import base.contract.IContext
import base.activity

private var toast: Toast? = null
private var alert: AlertDialog? = null
private var alertBuilder: AlertDialog.Builder? = null

private var alertText = ""

@SuppressLint("ShowToast")
fun IContext.toast(message: Any, show: Boolean = true): Toast {
    toast = toast?.apply { toast?.setText(activity.stringAny(message)) }
        ?: Toast.makeText(activity, activity.stringAny(message), Toast.LENGTH_SHORT)
    if (show) toast?.show()
    return toast!!
}

fun IContext.snack(message: Any, show: Boolean = true): Snackbar {
    val snack = Snackbar.make(
        activity.contentView,
        activity.stringAny(message),
        Snackbar.LENGTH_LONG
    )
    if (show) snack.show()
    return snack
}

fun IContext.alert(
    message: Any,
    show: Boolean = true,
    build: AlertDialog.Builder.() -> Unit = {}
): AlertDialog {
    val newText = activity.stringAny(message)
    alertBuilder = alertBuilder?.apply {
        if (alert != null && alert!!.isShowing && newText == alertText) return alert!!
        alert?.setMessage(newText)
    } ?: AlertDialog.Builder(activity).setMessage(newText)
    alertText = newText
    alertBuilder?.run {
        build()
        alert = create()
        if (show) alert?.show()
    }
    return alert!!
}

private fun Context.stringAny(text: Any?): String = when (text) {
    is String -> text
    is CharSequence -> text.toString()
    is Pair<*, *> -> stringAny(text.first) + " " + stringAny(text.second)
    is Int -> getResourceOrToString(text)
    else -> ""
}

private fun Context.getResourceOrToString(text: Int) = try {
    getString(text)
} catch (ex: Resources.NotFoundException) {
    text.toString()
}


private val Activity.contentView get() = findViewById<ViewGroup>(android.R.id.content).getChildAt(0)