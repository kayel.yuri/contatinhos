package base.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding

abstract class FragBind<Binding : ViewDataBinding> : FragBase<View>() {

    private var _binding: Binding? = null

    val binding: Binding
        get() = _binding ?: run {
            _binding = onBind()
            _binding!!
        }

    abstract fun onBind() : Binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, state: Bundle?) =
        binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.onBoundView()
    }

    abstract fun Binding.onBoundView()

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}