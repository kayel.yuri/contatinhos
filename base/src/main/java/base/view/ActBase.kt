package base.view

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import contatinhos.base.R
import base.contract.IPermissionRequest
import base.contract.IPermissionResult
import base.contract.IContext

open class ActBase(private val layout: Any = R.layout.frame) :
    AppCompatActivity(), IPermissionResult, IContext {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent?.extras?.onExtras()

        when(layout) {
            is Int -> {
                setContentView(layout)
                ((window.decorView.rootView as ViewGroup).getChildAt(0) as ViewGroup).onView()
            }
            is ViewGroup -> {
                setContentView(layout)
                layout.onView()
            }
            is View -> {
                setContentView(layout)
            }
        }
    }

    open fun Bundle.onExtras() {}

    open fun ViewGroup.onView() {}

    override var iPermissionRequest: IPermissionRequest? = null

    override fun onRequestPermissionsResult(
        code: Int,
        permissions: Array<out String>,
        results: IntArray
    ) {
        super.onRequestPermissionsResult(code, permissions, results)
        requestPermissionsResult(code, permissions, results)
    }
}
