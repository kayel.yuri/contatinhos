package base.view

import android.os.Bundle
import androidx.databinding.ViewDataBinding

abstract class ActBind<Binding : ViewDataBinding> : ActBase() {
    
    private var _binding: Binding? = null

    private val binding: Binding
        get() = _binding ?: run {
            _binding = onBind()
            _binding!!
        }

    abstract fun onBind() : Binding
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.lifecycleOwner = this
        binding.onBoundView()
    }
    
    open fun Binding.onBoundView() {}
    
    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}
