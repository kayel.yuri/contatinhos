package base.view

import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.LayoutParams.MATCH_PARENT
import androidx.recyclerview.widget.RecyclerView.LayoutParams.WRAP_CONTENT
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import base.addOnScrollStateChanged
import base.contract.IContext
import kotlin.reflect.KClass

inline fun <reified Builder : ItemViewBuilder<*, *>> RecyclerView.buildRecyclerAdapter(
    collection: Collection<*>,
    buffer: Int = 20,
    noinline setup: (RecyclerView.(RecyclerAdapter) -> Unit)? = null
) = object : RecyclerAdapter(collection, buffer) {

    init {
        init()
        setup?.invoke(this@buildRecyclerAdapter, this)
    }

    fun init() {
        onTarget?.let { addOnScrollStateChanged { if (!canScrollVertically(1)) it() } }
        adapter = this
    }

    override fun onCreateViewHolder(recycler: ViewGroup, viewType: Int) =
        RecyclerViewHolder(Builder::class.java.newInstance().init(recycler, collection))

    override fun getItemCount() = collection.size

    override fun onBindViewHolder(viewHolder: RecyclerViewHolder, position: Int) {
        if (position == target) onTarget?.invoke(this)
        viewHolder.builder.onBind(position)
    }
}

abstract class RecyclerAdapter(
    val collection: Collection<*>,
    val buffer: Int,
    open var onTarget: (RecyclerAdapter.() -> Unit)? = null
) : Adapter<RecyclerViewHolder>() {
    val target get() = collection.size - buffer
}

class RecyclerViewHolder(val builder: ItemViewBuilder<*, *>) : ViewHolder(builder.getView())

abstract class ItemViewBuilder<Data, Binding : ViewDataBinding>
    (
    private val bindClass: KClass<Binding>,
    private val orientation: Int = VERTICAL
) : IContext {

    lateinit var recycler: RecyclerView
    lateinit var collection: Collection<Data>
    lateinit var binding: Binding

    @Suppress("UNCHECKED_CAST")
    fun init(group: ViewGroup, coll: Collection<*>) = apply {
        recycler = group as RecyclerView
        collection = coll as Collection<Data>
    }

    fun getView(): View {
        binding = bind(bindClass)
        return binding.root.apply {
            layoutParams = if (orientation == VERTICAL) {
                RecyclerView.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            } else {
                RecyclerView.LayoutParams(WRAP_CONTENT, MATCH_PARENT)
            }
        }
    }

    fun onBind(position: Int) = binding.onBind(position)

    abstract fun Binding.onBind(position: Int)
}