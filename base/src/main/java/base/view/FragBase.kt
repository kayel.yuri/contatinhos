package base.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import contatinhos.base.R
import base.contract.IContext

abstract class FragBase<T: View> : Fragment(), IContext {
    
    open val contentView: T? = null
    open val layout: Int = R.layout.frame
    
    companion object {
        fun <Fragment : FragBase<*>> Fragment.with(bundle: Bundle? = null): Fragment {
            arguments = bundle
            return this
        }
    }
    
    @Suppress("UNCHECKED_CAST")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, state: Bundle?):
            T = contentView ?: inflater.inflate(layout, container, false) as T
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.onArguments()
        contentView?.onView()
    }
    
    open fun Bundle.onArguments() {}
    
    open fun T.onView() {}
}