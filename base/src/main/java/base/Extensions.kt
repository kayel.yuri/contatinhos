package base

import android.annotation.SuppressLint
import android.content.Context
import android.content.ContextWrapper
import android.content.res.TypedArray
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import base.Steve.main
import base.contract.IActContract
import base.view.FragBase
import base.view.FragBase.Companion.with
import kotlinx.coroutines.delay

val Context.activity: AppCompatActivity
    get() = when (this) {
        is AppCompatActivity -> this
        else -> (this as ContextWrapper).baseContext.activity
    }

val Context.isOnline
    @SuppressLint("MissingPermission")
    get() =
        (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?)?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                it.getNetworkCapabilities(it.activeNetwork)?.run {
                    return@let hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                            hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                            hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                }
            } else {
                @Suppress("DEPRECATION")
                it.activeNetworkInfo?.isConnectedOrConnecting
            }
        } ?: false

inline fun <reified Fragment : FragBase<*>> IActContract.loadFrag(
    bundle: Bundle? = null,
    backStack: Boolean = true
) {
    appCompatActivity.main {
        appCompatActivity.supportFragmentManager.run {
            delay(50)
            val fragName = Fragment::class.java.name
            if (!popBackStackImmediate(fragName, 0)) {
                beginTransaction().run {
                    if (backStack) {
                        addToBackStack(fragName)
                    }
                    replace(container, Fragment::class.java.newInstance().with(bundle), fragName)
                    commit()
                }
            }
        }
    }
}

fun RecyclerView.addOnScrollStateChanged(block: RecyclerView.(newState: Int) -> Unit) =
    addOnScrollListener(
        object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) =
                block(recyclerView, newState)
        }
    )

fun RecyclerView.addOnScrolled(block: RecyclerView.(dx: Int, dy: Int) -> Unit) =
    addOnScrollListener(
        object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) =
                block(recyclerView, dx, dy)
        }
    )

fun View.obtainStyledAttributes(
    set: AttributeSet?,
    attrRes: Int = 0,
    styleRes: Int = 0,
    styleableRes: IntArray = intArrayOf(),
    setup: TypedArray.() -> Unit = {}
) {
    if (set != null) {
        context.obtainStyledAttributes(set, styleableRes, attrRes, styleRes).run {
            setup.invoke(this)
            recycle()
        }
    }
}