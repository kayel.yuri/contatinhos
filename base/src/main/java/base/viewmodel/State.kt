package base.viewmodel

sealed class State {
    object Loading: State()
    object Failure: State()
    object Success: State()
}