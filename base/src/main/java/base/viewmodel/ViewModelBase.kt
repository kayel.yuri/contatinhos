package base.viewmodel

import androidx.lifecycle.ViewModel

abstract class ViewModelBase<S : ViewState<State>, A : ViewAction> : ViewModel() {
    abstract val viewState: S
    open fun dispatchAction(action: A) {}
}