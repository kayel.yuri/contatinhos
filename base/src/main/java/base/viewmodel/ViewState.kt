package base.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

abstract class ViewState<S : State>{

    val state: MutableLiveData<S> = MutableLiveData()

    val isSuccess = Transformations.map(state) { state -> state == State.Success }
    val isLoading = Transformations.map(state) { state -> state == State.Loading }
    val isError = Transformations.map(state) { state -> state == State.Failure }

}