package base

import android.app.Application
import android.app.Dialog
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Dispatchers.Unconfined
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import base.view.ItemViewBuilder

object Steve {

    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        Log.e(
            "CoroutineEx",
            exception.stackTraceToString()
        )
    }

    private fun <Scope> Scope.identifyScope() = when (this) {
        is ViewModel                -> viewModelScope
        is AppCompatActivity        -> lifecycleScope
        is Fragment                 -> viewLifecycleOwner.lifecycleScope
        is ItemViewBuilder<*, *>    -> activity.lifecycleScope
        is Dialog                   -> context.activity.lifecycleScope
        is View                     -> context.activity.lifecycleScope
        is Application              -> GlobalScope
        else                        -> CoroutineScope(Main.immediate)
    }

    private fun <Result> CoroutineContext.launch(
        coroutine: suspend CoroutineScope.(Job) -> Result,
        scope: CoroutineScope = CoroutineScope(this),
        job: Job = Job()
    ) = scope.launch(this + job + exceptionHandler) {
        coroutine.invoke(this, job)
    }

    private fun <Result> CoroutineContext.async(
        coroutine: suspend CoroutineScope.(Job) -> Result,
        scope: CoroutineScope = CoroutineScope(this),
        job: Job = Job()
    ) = scope.async(this + job + exceptionHandler) {
        coroutine.invoke(this, job)
    }

    fun <Scope, Result> Scope.default(coroutine: suspend CoroutineScope.(Job) -> Result) =
        Default.launch(coroutine, identifyScope())

    fun <Scope, Result> Scope.main(coroutine: suspend CoroutineScope.(Job) -> Result) =
        Main.launch(coroutine, identifyScope())

    fun <Scope, Result> Scope.unconfined(coroutine: suspend CoroutineScope.(Job) -> Result) =
        Unconfined.launch(coroutine, identifyScope())

    fun <Scope, Result> Scope.io(coroutine: suspend CoroutineScope.(Job) -> Result) =
        IO.launch(coroutine, identifyScope())
}