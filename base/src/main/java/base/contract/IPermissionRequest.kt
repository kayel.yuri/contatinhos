package base.contract

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager.PERMISSION_GRANTED
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat.checkSelfPermission

interface IPermissionRequest : IContext {

    val requestCode get() = 666

    fun requestPermission(permission: String = WRITE_EXTERNAL_STORAGE) =
        if (checkSelfPermission(activity, permission) == PERMISSION_GRANTED) {
            onPermissionGranted(permission)
            true
        } else {
            onRequest(permission)
            false
        }

    private fun onRequest(permission: String) {
        (activity as base.contract.IPermissionResult).iPermissionRequest = this
        requestPermissions(activity, arrayOf(permission), requestCode)
        onPermissionRequested(permission)
    }

    fun onPermissionRequested(permission: String) {}

    fun onPermissionGranted(permission: String) {}

    fun onPermissionDenied(permission: String) {}
}