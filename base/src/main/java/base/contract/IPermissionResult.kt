package base.contract

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager.PERMISSION_GRANTED
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat.checkSelfPermission

interface IPermissionResult {
    var iPermissionRequest: IPermissionRequest?

    fun requestPermissionsResult(
        code: Int,
        permissions: Array<out String>,
        results: IntArray
    ) {
        if (code == iPermissionRequest?.requestCode && results.isNotEmpty()) {
            iPermissionRequest?.run {
                if (results[0] == PERMISSION_GRANTED) {
                    onPermissionGranted(permissions[0])
                } else {
                    onPermissionDenied(permissions[0])
                }
            }
        }
    }
}

