package base.contract

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import contatinhos.base.R

interface IActContract {

    val appCompatActivity get() = this as AppCompatActivity

    val container: Int get() = R.id.default_frag_container

    fun dispatchTo(id: Int, bundle: Bundle? = null) {}

    fun onItemClick(id: Int) {}

    fun backPress() = appCompatActivity.onBackPressed()

    fun popBackStack() = appCompatActivity.supportFragmentManager.popBackStack()

    fun setLoading(loading: Boolean) {}
}

