package base.contract

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import base.activity
import base.view.ItemViewBuilder
import kotlin.reflect.KClass

interface IContext {

    val activity
        get() : AppCompatActivity = when (this) {
            is Fragment -> requireContext().activity
            is View -> context.activity
            is ItemViewBuilder<*, *> -> recycler.context.activity
            is Dialog -> context.activity
            else -> this as AppCompatActivity
        }

    @Suppress("UNCHECKED_CAST")
    fun <Binding : ViewDataBinding> bind(klass: KClass<Binding>) =
        klass.java.getMethod("inflate", LayoutInflater::class.java)
            .invoke(null, inflater) as Binding

    val bundle
        get() : Bundle =
            (if (this is Activity) intent?.extras else (this as Fragment).arguments) ?: Bundle()

    val inflater get() = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
}