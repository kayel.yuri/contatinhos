@file:Suppress("FunctionName")

package base.error

import retrofit2.Response

open class Failure(private val _message: String?, val throwable: Throwable? = null) {
    val message get() = _message ?: "Unexpected error ¯\\_(ツ)_/¯"
}

fun <T> Response<T>.ResponseFailure() = Failure("${code()}:${message()}\n\n${errorBody()}")