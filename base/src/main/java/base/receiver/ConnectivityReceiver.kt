package base.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import base.isOnline

private const val DEPRECATED_CONN_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE"

class ConnectivityReceiver : BroadcastReceiver(), IConnectivity, LifecycleObserver {

    override val hasConnectivity = MutableLiveData<Boolean>()

    private var act: AppCompatActivity? = null

    override fun onReceive(context: Context, intent: Intent?) {
        hasConnectivity.value = context.isOnline

        Toast.makeText(context, if (context.isOnline) "ONLINE!" else "OFF!", Toast.LENGTH_LONG)
            .show()
    }

    override fun subscribe(act: AppCompatActivity) {
        act.lifecycle.addObserver(this)
        this.act = act
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        if (Build.VERSION.SDK_INT < 30) {
            act?.registerReceiver(this, IntentFilter(DEPRECATED_CONN_CHANGE))
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        act?.unregisterReceiver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        act = null
    }
}