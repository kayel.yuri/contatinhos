package base.receiver

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData

interface IConnectivity {

    val hasConnectivity : MutableLiveData<Boolean>
    val isOnline get() = hasConnectivity.value == true

    fun subscribe(act : AppCompatActivity)
}