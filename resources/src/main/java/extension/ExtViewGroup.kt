package extension

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.bind(layoutID: Int): View = View.inflate(context, layoutID, this)

fun ViewGroup.inflateDetached(layoutID: Int): View =
    LayoutInflater.from(context).inflate(layoutID, null, false)

fun ViewGroup.addViewsAtEnd(
    vararg arrayOfViews: Any?,
    also: (Int, View) -> Unit = { _, _ -> }
) = addViews(arrayOfViews, childCount, also)

fun ViewGroup.addViewsAt(
    atPosition: Int = 0,
    vararg arrayOfViews: Any?,
    also: (Int, View) -> Unit = { _, _ -> }
) = addViews(arrayOfViews, atPosition, also)

fun ViewGroup.addViews(
    vararg arrayOfViews: Any?,
    atPosition: Int = 0,
    also: (Int, View) -> Unit = { _, _ -> }
): ViewGroup {
    val previousChildCount = childCount
    arrayOfViews.forEachIndexed { index, any ->
        if (any is View) {
            addView(any, childCount - previousChildCount + atPosition)
            also.invoke(index, any)
        }
    }
    return this
}
