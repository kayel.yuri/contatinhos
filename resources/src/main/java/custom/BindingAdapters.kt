package custom

import android.view.View
import android.view.View.GONE
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import androidx.databinding.BindingAdapter

@BindingAdapter("isVisible")
fun View.setVisible(visible: Boolean) {
    visibility = if (visible) VISIBLE else INVISIBLE
}

@BindingAdapter("isGone")
fun View.setGone(gone: Boolean) {
    visibility = if (gone) GONE else VISIBLE
}

@BindingAdapter("scrollState")
fun ScrollObserverView.setScrollState(state: IScrollState) {
    scrollState = state
}