package custom

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import ext_context.observeLifecycle
import extension.toast

/*
    Work in Progress
*/

class ScrollObserverView @JvmOverloads constructor(
    context: Context,
    set: AttributeSet? = null,
    attrRes: Int = 0,
    construct: ScrollObserverView.() -> Unit = {}
) : NestedScrollView(context, set, attrRes), LifecycleObserver {

    var scrollState: IScrollState? = null

    init {
        observeLifecycle()
        construct()
    }

    public override fun onSaveInstanceState(): Parcelable? =
        super.onSaveInstanceState()

    public override fun onRestoreInstanceState(state: Parcelable?) =
        super.onRestoreInstanceState(state)

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        toast("ON RESUME")
        scrollTo(scrollState?.scrollX ?: 0, scrollState?.scrollY ?: 0)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        toast("ON PAUSE")
        scrollState?.scrollX = scrollX
        scrollState?.scrollY = scrollY
    }
}