package custom

import android.content.Context
import android.util.AttributeSet
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView

open class RecyclerShyView @JvmOverloads constructor(
    context: Context,
    set: AttributeSet? = null,
    styleAttr: Int = 0
) : RecyclerView(context, set, styleAttr) {

    init {
        isVisible = adapter != null
    }

    override fun setAdapter(adapter: Adapter<*>?) {
        isVisible = adapter != null
        if (isVisible) super.setAdapter(adapter)
    }
}