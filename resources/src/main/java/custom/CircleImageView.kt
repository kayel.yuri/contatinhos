package custom

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.Shader
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.AttributeSet
import android.widget.ImageView
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import androidx.annotation.DrawableRes
import contatinhos.res.R
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt

private const val DEF_PRESS_HIGHLIGHT_COLOR = 0x32000000

@SuppressLint("AppCompatCustomView")
open class CircleImageView @JvmOverloads
constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : ImageView(context, attrs, defStyleAttr, defStyleRes) {

    private var bitmapShader: Shader? = null
    private val shaderMatrix: Matrix
    private val bitmapDrawBounds: RectF
    private val strokeBounds: RectF
    private var bitmap: Bitmap? = null
    private val bitmapPaint: Paint
    private val strokePaint: Paint
    private val pressedPaint: Paint
    private val initialized: Boolean
    private var isStatePressed = false
    private var isHighlightEnabled: Boolean

    override fun setImageResource(@DrawableRes resId: Int) {
        super.setImageResource(resId)
        setupBitmap()
    }

    override fun setImageDrawable(drawable: Drawable?) {
        super.setImageDrawable(drawable)
        setupBitmap()
    }

    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)
        setupBitmap()
    }

    override fun setImageURI(uri: Uri?) {
        super.setImageURI(uri)
        setupBitmap()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        val halfStrokeWidth: Float = strokePaint.strokeWidth / 2f
        updateCircleDrawBounds(bitmapDrawBounds)
        strokeBounds.set(bitmapDrawBounds)
        strokeBounds.inset(halfStrokeWidth, halfStrokeWidth)
        updateBitmapSize()
    }

    override fun onDraw(canvas: Canvas) {
        drawBitmap(canvas)
        drawStroke(canvas)
        drawHighlight(canvas)
    }

    var isHighlightEnable: Boolean
        get() = isHighlightEnabled
        set(enable) {
            isHighlightEnabled = enable
            invalidate()
        }

    @get:ColorInt
    var highlightColor: Int
        get() = pressedPaint.color
        set(color) {
            pressedPaint.color = color
            invalidate()
        }

    @get:ColorInt
    var strokeColor: Int
        get() = strokePaint.color
        set(color) {
            strokePaint.color = color
            invalidate()
        }

    @get:Dimension
    var strokeWidth: Float
        get() = strokePaint.strokeWidth
        set(width) {
            strokePaint.strokeWidth = width
            invalidate()
        }

    private fun drawHighlight(canvas: Canvas) {
        if (isHighlightEnabled && isStatePressed) {
            canvas.drawOval(bitmapDrawBounds, pressedPaint)
        }
    }

    private fun drawStroke(canvas: Canvas) {
        if (strokePaint.strokeWidth > 0f) {
            canvas.drawOval(strokeBounds, strokePaint)
        }
    }

    private fun drawBitmap(canvas: Canvas) {
        canvas.drawOval(bitmapDrawBounds, bitmapPaint)
    }

    private fun updateCircleDrawBounds(bounds: RectF) {
        val contentWidth: Float = (width - paddingLeft - paddingRight).toFloat()
        val contentHeight: Float = (height - paddingTop - paddingBottom).toFloat()
        var left: Float = paddingLeft.toFloat()
        var top: Float = paddingTop.toFloat()
        if (contentWidth > contentHeight) {
            left += (contentWidth - contentHeight) / 2f
        } else {
            top += (contentHeight - contentWidth) / 2f
        }
        val diameter = min(contentWidth, contentHeight)
        bounds[left, top, left + diameter] = top + diameter
    }

    private fun setupBitmap() {
        if (!initialized) return
        bitmap = getBitmapFromDrawable(drawable)
        if (bitmap == null) return
        bitmapShader =
            BitmapShader(bitmap!!, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        bitmapPaint.shader = bitmapShader
        updateBitmapSize()
    }

    private fun updateBitmapSize() {
        if (bitmap == null) return
        val dx: Float
        val dy: Float
        val scale: Float

        if (bitmap!!.width < bitmap!!.height) {
            scale = bitmapDrawBounds.width() / bitmap!!.width.toFloat()
            dx = bitmapDrawBounds.left
            dy =
                bitmapDrawBounds.top - bitmap!!.height * scale / 2f + bitmapDrawBounds.width() / 2f
        } else {
            scale = bitmapDrawBounds.height() / bitmap!!.height.toFloat()
            dx =
                bitmapDrawBounds.left - bitmap!!.width * scale / 2f + bitmapDrawBounds.width() / 2f
            dy = bitmapDrawBounds.top
        }
        shaderMatrix.setScale(scale, scale)
        shaderMatrix.postTranslate(dx, dy)
        bitmapShader!!.setLocalMatrix(shaderMatrix)
    }

    private fun getBitmapFromDrawable(drawable: Drawable?): Bitmap? {
        if (drawable == null) return null
        if (drawable is BitmapDrawable) return drawable.bitmap
        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    private fun isInCircle(x: Float, y: Float): Boolean {
        val distance = sqrt(
            (bitmapDrawBounds.centerX() - x.toDouble()).pow(2.0) +
                    (bitmapDrawBounds.centerY() - y.toDouble()).pow(2.0)
        )
        return distance <= bitmapDrawBounds.width() / 2
    }

    init {
        var strokeColor: Int = Color.TRANSPARENT
        var strokeWidth = 0f
        var highlightEnable = true
        var highlightColor = DEF_PRESS_HIGHLIGHT_COLOR
        if (attrs != null) {
            val array: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.CircleImageView, 0, 0)

            strokeColor = array.getColor(
                R.styleable.CircleImageView_colorStroke,
                Color.TRANSPARENT
            )

            strokeWidth =
                array.getDimensionPixelSize(R.styleable.CircleImageView_widthStroke, 0)
                    .toFloat()

            highlightEnable =
                array.getBoolean(R.styleable.CircleImageView_highlightEnable, true)

            highlightColor = array.getColor(
                R.styleable.CircleImageView_highlightColor,
                DEF_PRESS_HIGHLIGHT_COLOR
            )
            array.recycle()
        }
        shaderMatrix = Matrix()
        bitmapPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        strokePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        strokeBounds = RectF()
        bitmapDrawBounds = RectF()
        strokePaint.color = strokeColor
        strokePaint.style = Paint.Style.STROKE
        strokePaint.strokeWidth = strokeWidth
        pressedPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        pressedPaint.color = highlightColor
        pressedPaint.style = Paint.Style.FILL
        isHighlightEnabled = highlightEnable
        initialized = true
        setupBitmap()
    }
}