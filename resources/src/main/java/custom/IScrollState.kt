package custom

interface IScrollState {
    var scrollY : Int
    var scrollX : Int
}