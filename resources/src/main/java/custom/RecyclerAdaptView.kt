package custom

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import contatinhos.res.R
import extension.obtainStyledAttributes

@Suppress("FunctionName")
fun <F : Fragment> F.RecyclerAdaptView(
    construct: RecyclerAdaptView.() -> Unit = {}
) = RecyclerAdaptView(requireContext(), construct = construct)

class RecyclerAdaptView @JvmOverloads constructor(
    context: Context,
    private val set: AttributeSet? = null,
    private val attrRes: Int = 0,
    private val construct: RecyclerAdaptView.() -> Unit = {}
) : RecyclerShyView(context, set, attrRes) {

    private var initialized: Boolean = false

    var orientation = VERTICAL
        set(orientation) {
            field = orientation
            if (initialized) setup()
        }

    var spanCount = 1
        set(spanCount) {
            field = spanCount
            if (initialized) setup()
        }

    var reverse = false
        set(reverseLayout) {
            field = reverseLayout
            if (initialized) setup()
        }

    init {
        setup()
    }

    private fun setup() {
        if (!initialized) {
            obtainStyledAttributes(set, attrRes, styleableRes = R.styleable.RecyclerAdaptView) {
                orientation = getInt(R.styleable.RecyclerAdaptView_orientation, orientation)
                spanCount = getInt(R.styleable.RecyclerAdaptView_spanCount, spanCount)
                reverse = getBoolean(R.styleable.RecyclerAdaptView_reverseLayout, reverse)
            }
        }

        layoutManager = when (spanCount) {
            1 -> LinearLayoutManager(context, orientation, reverse)
            else -> GridLayoutManager(context, orientation, spanCount, reverse)
        }

        if (set == null) applyDefaultLayoutParams()

        if (childCount == 1) contentDescription = getChildAt(0).contentDescription

        initialized = true
        construct.invoke(this)
    }

    private fun applyDefaultLayoutParams() {
        layoutParams = if (orientation == VERTICAL) {
            LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        } else {
            LayoutParams(WRAP_CONTENT, MATCH_PARENT)
        }
    }
}