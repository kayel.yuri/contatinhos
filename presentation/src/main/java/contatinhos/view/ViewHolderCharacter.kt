package contatinhos.view

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import contatinhos.presentation.R
import custom.CircleImageView
import data.entity.EntityCharList

class ViewHolderCharacter(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val species by lazy { itemView.findViewById<TextView>(R.id.subtitle) }
    private val name by lazy { itemView.findViewById<TextView>(R.id.title) }
    private val progressBar by lazy { itemView.findViewById<ProgressBar>(R.id.progressBar) }
    private val picture by lazy { itemView.findViewById<CircleImageView>(R.id.picture) }

    fun bind(character: EntityCharList.Character) {
        species.text = character.species
        name.text = character.name
        progressBar.visibility = View.VISIBLE
        Picasso.get()
            .load(character.image)
            .error(R.drawable.ic_round_account_circle)
            .into(picture, object : Callback {
                override fun onSuccess() {
                    progressBar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    progressBar.visibility = View.GONE
                }
            })
    }
}