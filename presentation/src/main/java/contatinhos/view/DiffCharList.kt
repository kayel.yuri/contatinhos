package contatinhos.view

import androidx.recyclerview.widget.DiffUtil
import data.entity.EntityCharList

class DiffCharList(
    private val oldList: List<EntityCharList.Character>,
    private val newList: List<EntityCharList.Character>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val old = oldList[oldItemPosition]
        val new = newList[newItemPosition]

        return old.name == new.name &&
                old.id == new.id &&
                old.created == new.created
    }
}