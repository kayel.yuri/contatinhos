package contatinhos.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import contatinhos.presentation.R
import data.entity.EntityEpisodeList

class AdapterEpisode : RecyclerView.Adapter<ViewHolderEpisode>() {

    var episodes = emptySet<EntityEpisodeList.Episode>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolderEpisode(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolderEpisode, position: Int) {
        if (episodes.isNotEmpty()) holder.bind(episodes.elementAt(position))
    }

    override fun getItemCount(): Int = episodes.size
}