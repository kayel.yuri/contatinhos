package contatinhos.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import contatinhos.presentation.R
import data.entity.EntityLocationList

class AdapterLocation : RecyclerView.Adapter<ViewHolderLocation>() {

    var locations = emptySet<EntityLocationList.Location>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolderLocation(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolderLocation, position: Int) {
        if (locations.isNotEmpty()) holder.bind(locations.elementAt(position))
    }

    override fun getItemCount(): Int = locations.size
}