package contatinhos.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import contatinhos.presentation.R
import data.entity.EntityCharList

class AdapterCharacter : RecyclerView.Adapter<ViewHolderCharacter>() {

    var characters = emptySet<EntityCharList.Character>()
        set(v) {
            DiffUtil.calculateDiff(DiffCharList(field.toList(), v.toList())).dispatchUpdatesTo(this)
            field = v
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolderCharacter(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolderCharacter, position: Int) {
        if (characters.isNotEmpty()) holder.bind(characters.elementAt(position))
    }

    override fun getItemCount(): Int = characters.size
}