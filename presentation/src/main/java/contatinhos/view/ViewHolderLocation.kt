package contatinhos.view

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import contatinhos.presentation.R
import custom.CircleImageView
import data.entity.EntityLocationList

class ViewHolderLocation(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val title by lazy { itemView.findViewById<TextView>(R.id.title) }
    private val subtitle by lazy { itemView.findViewById<TextView>(R.id.subtitle) }
    private val progressBar by lazy { itemView.findViewById<ProgressBar>(R.id.progressBar) }
    private val picture by lazy { itemView.findViewById<CircleImageView>(R.id.picture) }

    fun bind(location: EntityLocationList.Location) {
        title.text = location.name
        val output =
            "${location.residents.random().replace("character/", "character/avatar/")}.jpeg"
        subtitle.text = location.type

        progressBar.visibility = View.VISIBLE
        Picasso.get()
            .load(output)
            .into(picture, object : Callback {
                override fun onSuccess() {
                    progressBar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    progressBar.visibility = View.GONE
                }
            })
    }
}