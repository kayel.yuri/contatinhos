package contatinhos.view

import base.Steve.main
import base.receiver.IConnectivity
import base.view.ActBind
import contatinhos.modules
import contatinhos.presentation.R
import contatinhos.presentation.databinding.ActMainBinding
import contatinhos.viewmodel.Action
import contatinhos.viewmodel.ViewModelCharacters
import contatinhos.viewmodel.ViewModelEpisodes
import contatinhos.viewmodel.ViewModelLocations
import ext.observe
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.unloadKoinModules

class ActRickMorty : ActBind<ActMainBinding>() {

    private val adapterCharacter: AdapterCharacter by lazy { AdapterCharacter() }
    private val adapterLocation: AdapterLocation by lazy { AdapterLocation() }
    private val adapterEpisode: AdapterEpisode by lazy { AdapterEpisode() }

    private val viewModelCharacters: ViewModelCharacters by viewModel()
    private val viewModelLocations: ViewModelLocations by viewModel()
    private val viewModelEpisodes: ViewModelEpisodes by viewModel()

    private val connectivityReceiver: IConnectivity by inject()

    override fun onBind() = ActMainBinding.inflate(inflater)

    override fun ActMainBinding.onBoundView() {
        vm = viewModelCharacters
        fetch = Action.Fetch
        connectivityReceiver.subscribe(this@ActRickMorty)

        recycler.adapter = adapterCharacter

        nav.setOnItemSelectedListener { item ->
            recycler.adapter = when (item.itemId) {
                R.id.nav_b -> adapterLocation
                R.id.nav_c -> adapterEpisode
                else -> adapterCharacter
            }
            true;
        }

        with(viewModelCharacters) {
            observe(viewState.characters) { adapterCharacter.characters = it.characters }
            dispatchAction(Action.Fetch)
        }


        with(viewModelLocations) {
            observe(viewState.locations) { adapterLocation.locations = it.locations }
            dispatchAction(Action.Fetch)
        }

        with(viewModelEpisodes) {
            observe(viewState.episodes) { adapterEpisode.episodes = it.episodes }
            dispatchAction(Action.Fetch)
        }
    }

    override fun onDestroy() {
        unloadKoinModules(modules)
        super.onDestroy()
    }
}
