package contatinhos.view

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import contatinhos.presentation.R
import custom.CircleImageView
import data.entity.EntityEpisodeList

class ViewHolderEpisode(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val title by lazy { itemView.findViewById<TextView>(R.id.title) }
    private val subtitle by lazy { itemView.findViewById<TextView>(R.id.subtitle) }
    private val progressBar by lazy { itemView.findViewById<ProgressBar>(R.id.progressBar) }
    private val picture by lazy { itemView.findViewById<CircleImageView>(R.id.picture) }

    fun bind(episode: EntityEpisodeList.Episode) {
        title.text = episode.name
        val output = "${episode.characters.random().replace("character/", "character/avatar/")}.jpeg"
        subtitle.text = output.replace("https://", "")

        progressBar.visibility = View.VISIBLE
        Picasso.get()
            .load(output)
            .error(R.drawable.ic_round_account_circle)
            .into(picture, object : Callback {
                override fun onSuccess() {
                    progressBar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    progressBar.visibility = View.GONE
                }
            })
    }
}