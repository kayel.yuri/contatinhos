package contatinhos.viewmodel

import base.Steve.main
import base.error.Failure
import base.viewmodel.State
import base.viewmodel.ViewModelBase
import data.entity.EntityCharList
import domain.usecases.UseCaseCharacters
import kotlinx.coroutines.delay

class ViewModelCharacters(
    private val useCaseCharacters: UseCaseCharacters
) : ViewModelBase<ViewStateCharacters, Action>() {

    override val viewState = ViewStateCharacters()
    private var currentPage = 1

    override fun dispatchAction(action: Action) {
        when (action) {
            is Action.Fetch -> fetch()
        }
    }

    private fun fetch() = main {
        viewState.state.value = State.Loading
        useCaseCharacters.execute(currentPage).fold(::onError, ::onSuccess)
    }

    private fun onError(error: Failure) = with(viewState) {
        main {
            delay(300)
            state.postValue(State.Failure)
            message.postValue(error.message)
        }
    }

    private fun onSuccess(list: EntityCharList) = with(viewState) {
        main {
            delay(300)
            state.postValue(State.Success)
            characters.postValue(list)
            message.postValue(list.characters.toString())
        }
    }

}