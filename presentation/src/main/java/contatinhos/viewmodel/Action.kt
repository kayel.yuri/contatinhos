package contatinhos.viewmodel

import base.viewmodel.ViewAction

sealed class Action : ViewAction() {
    object Fetch : Action()
}