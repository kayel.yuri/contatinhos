package contatinhos.viewmodel

import base.Steve.main
import base.error.Failure
import base.viewmodel.State
import base.viewmodel.ViewModelBase
import data.entity.EntityEpisodeList
import domain.usecases.UseCaseEpisodes
import kotlinx.coroutines.delay

class ViewModelEpisodes(
    private val useCaseEpisodes: UseCaseEpisodes
) : ViewModelBase<ViewStateEpisodes, Action>() {

    override val viewState = ViewStateEpisodes()
    private var currentPage = 1

    override fun dispatchAction(action: Action) {
        when (action) {
            is Action.Fetch -> fetch()
        }
    }

    private fun fetch() = main {
        viewState.state.value = State.Loading
        useCaseEpisodes.execute(currentPage).fold(::onError, ::onSuccess)
    }

    private fun onError(error: Failure) = with(viewState) {
        main {
            delay(300)
            state.postValue(State.Failure)
            message.postValue(error.message)
        }
    }

    private fun onSuccess(list: EntityEpisodeList) = with(viewState) {
        main {
            delay(300)
            state.postValue(State.Success)
            episodes.postValue(list)
            message.postValue(list.episodes.toString())
        }
    }

}