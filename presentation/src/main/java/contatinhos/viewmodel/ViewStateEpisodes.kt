package contatinhos.viewmodel

import androidx.lifecycle.MutableLiveData
import base.viewmodel.State
import base.viewmodel.ViewState
import data.entity.EntityEpisodeList

class ViewStateEpisodes : ViewState<State>() {
    val episodes = MutableLiveData<EntityEpisodeList>()
    val message = MutableLiveData<Any>()
}