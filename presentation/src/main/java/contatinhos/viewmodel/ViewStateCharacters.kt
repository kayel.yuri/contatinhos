package contatinhos.viewmodel

import androidx.lifecycle.MutableLiveData
import base.viewmodel.State
import base.viewmodel.ViewState
import data.entity.EntityCharList

class ViewStateCharacters : ViewState<State>() {
    val characters = MutableLiveData<EntityCharList>()
    val message = MutableLiveData<Any>()
}