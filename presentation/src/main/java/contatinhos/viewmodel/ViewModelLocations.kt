package contatinhos.viewmodel

import base.Steve.main
import base.error.Failure
import base.viewmodel.State
import base.viewmodel.ViewModelBase
import data.entity.EntityLocationList
import domain.usecases.UseCaseLocations
import kotlinx.coroutines.delay

class ViewModelLocations(
    private val useCaseLocations: UseCaseLocations
) : ViewModelBase<ViewStateLocations, Action>() {

    override val viewState = ViewStateLocations()
    private var currentPage = 1

    override fun dispatchAction(action: Action) {
        when (action) {
            is Action.Fetch -> fetch()
        }
    }

    private fun fetch() = main {
        viewState.state.value = State.Loading
        useCaseLocations.execute(currentPage).fold(::onError, ::onSuccess)
    }

    private fun onError(error: Failure) = with(viewState) {
        main {
            delay(300)
            state.postValue(State.Failure)
            message.postValue(error.message)
        }
    }

    private fun onSuccess(list: EntityLocationList) = with(viewState) {
        main {
            delay(300)
            state.postValue(State.Success)
            locations.postValue(list)
            message.postValue(list.locations.toString())
        }
    }

}