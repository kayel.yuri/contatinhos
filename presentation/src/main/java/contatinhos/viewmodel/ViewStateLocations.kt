package contatinhos.viewmodel

import androidx.lifecycle.MutableLiveData
import base.viewmodel.State
import base.viewmodel.ViewState
import data.entity.EntityLocationList

class ViewStateLocations : ViewState<State>() {
    val locations = MutableLiveData<EntityLocationList>()
    val message = MutableLiveData<Any>()
}