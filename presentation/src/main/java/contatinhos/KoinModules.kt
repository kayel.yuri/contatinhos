package contatinhos

import base.receiver.ConnectivityReceiver
import base.receiver.IConnectivity
import contatinhos.viewmodel.ViewModelCharacters
import contatinhos.viewmodel.ViewModelEpisodes
import contatinhos.viewmodel.ViewModelLocations
import data.repository.IRepositoryRM
import data.repository.RepositoryRM
import data.source.ISourceRM
import data.source.SourceRM
import domain.usecases.UseCaseCharacters
import domain.usecases.UseCaseEpisodes
import domain.usecases.UseCaseLocations
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

val modules by lazy {
    listOf(
        appContext,
        *receivers,
        *viewModels,
        *useCases,
        *repos,
        *sources
    )
}

private val appContext = module {
    single(named("appContext")) { androidContext() }
}

// region Receiver
private val networkReceiver = module { single<IConnectivity> { ConnectivityReceiver() } }
private val receivers get() = arrayOf(networkReceiver)
// endregion

// region ViewModel
private val viewModelCharacters = module { factory { ViewModelCharacters(get()) } }
private val viewModelLocations = module { factory { ViewModelLocations(get()) } }
private val viewModelEpisodes = module { factory { ViewModelEpisodes(get()) } }
private val viewModels get() = arrayOf(viewModelCharacters, viewModelLocations, viewModelEpisodes)
// endregion

// region UseCase
private val useCaseCharacters = module { single { UseCaseCharacters(get()) } }
private val useCaseLocations = module { single { UseCaseLocations(get()) } }
private val useCaseEpisodes = module { single { UseCaseEpisodes(get()) } }
private val useCases get() = arrayOf(useCaseCharacters, useCaseLocations, useCaseEpisodes)
// endregion

// region Repo
private val repositoryRM = module { single<IRepositoryRM> { RepositoryRM(get()) } }
private val repos get() = arrayOf(repositoryRM)
// endregion

// region Source
private val sourceRickAndMorty = module {
    single<ISourceRM> { SourceRM(get(), androidContext().cacheDir) }
}
private val sources get() = arrayOf(sourceRickAndMorty)
// endregion
