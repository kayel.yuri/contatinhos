package ext

import android.content.res.Resources

private val metrics get() = Resources.getSystem().displayMetrics

@Suppress("UNCHECKED_CAST") // Converts Pixel value to DensityPixel value
val <N : Number> N.dp get() = (toFloat() * metrics.density) as N

@Suppress("UNCHECKED_CAST") // Converts DensityPixel value to Pixel value
val <N : Number> N.px get() = (toFloat() / metrics.density) as N