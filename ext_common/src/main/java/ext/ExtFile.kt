package ext

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider.getUriForFile
import java.io.File
import java.io.FileOutputStream

private const val APP_AUTH = "kotlinfun.app.provider"

fun File.getURI(context: Context): Uri = getURIForFileWithAuth(context)

fun File.getURIForFileWithAuth(context: Context): Uri =
    getUriForFile(context, APP_AUTH, this)

fun File.writeHTML(html: String) = FileOutputStream(this).apply {
    write(html.toByteArray())
    flush()
    close()
}