package ext

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, block: (T) -> Unit) =
    liveData.observe(this, block)

fun <T> LifecycleOwner.observe(liveData: MutableLiveData<T>, block: (T) -> Unit) =
    liveData.observe(this, block)