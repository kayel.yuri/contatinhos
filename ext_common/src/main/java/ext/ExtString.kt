package ext

import java.text.SimpleDateFormat
import java.util.Locale
import java.util.Locale.getDefault

const val LINE_BREAK = "\n"
const val EMPTY_STRING = ""
const val DOT = "."
const val TAB_SPACE = "    "
const val DASH = "-"
const val BAR = "/"
const val ZERO = 0
const val TWELVE = 12
const val TWENTY_FOUR = 24
const val THIRTY_SIX = 36
const val FORTY_EIGHT = 48
const val TWO = 2
const val FOUR = 4
const val NINE = 9
const val SIXTEEN = 16
const val TYPE_PERSON = "F"
const val DIFF_CPF_CNPJ = 3

fun String.formatAsBarCode(withDigit: Boolean = false) = StringBuilder().also {
    val modifier = if (withDigit) 0 else -1
    if (length > TWELVE) {
        it.append(substring(ZERO, TWELVE + modifier))
        it.append(TAB_SPACE)
        if (length > TWENTY_FOUR) {
            it.append(substring(TWELVE, TWENTY_FOUR + modifier))
            it.append(LINE_BREAK)
            if (length > THIRTY_SIX) {
                it.append(substring(TWENTY_FOUR, THIRTY_SIX + modifier))
                it.append(TAB_SPACE)
                if (length >= FORTY_EIGHT) {
                    it.append(substring(THIRTY_SIX, FORTY_EIGHT + modifier))
                }
            }
        }
    }
}.toString()

fun String.formatAsDocNumber() = StringBuilder().also {
    if (length > TWO) {
        it.append(substring(ZERO, TWO))
        it.append(DOT)
        if (length > FOUR) {
            it.append(substring(TWO, FOUR))
            it.append(DOT)
            if (length > NINE) {
                it.append(substring(FOUR, NINE))
                it.append(DOT)
                if (length > SIXTEEN) {
                    it.append(substring(NINE, SIXTEEN))
                    it.append(DASH)
                    it.append(substring(SIXTEEN, length))
                }
            }
        }
    }
}.toString()

fun String.formatAsMonthYear() = StringBuilder().also {
    if (length > FOUR) {
        it.append(substring(FOUR, length))
        it.append(BAR)
        it.append(substring(ZERO, FOUR))
    }
}.toString()

fun String.parseDate(
    inputPattern: String = "yyyy-MM-dd",
    desiredOutputPattern: String = "dd/MM/yyyy"
): String =
    SimpleDateFormat(desiredOutputPattern, getDefault()).format(
        SimpleDateFormat(
            inputPattern,
            getDefault()
        ).parse(this)!!
    )

fun String.parseDateToSendInRequest(
    inputPattern: String = "dd/MM/yyyy",
    desiredOutputPattern: String = "yyyy-MM-dd"
): String =
    SimpleDateFormat(desiredOutputPattern, getDefault()).format(
        SimpleDateFormat(
            inputPattern,
            getDefault()
        ).parse(this)!!
    )

fun String.addSpacesBetweenCharacters(): String =
    if (isNullOrEmpty()) EMPTY_STRING else replace("(.)".toRegex(), "$1 ")

val <T> T.string get() = toString()

fun String.formatToCPF() =
    "${substring(0, 3)}$DOT${substring(3, 6)}$DOT${substring(6, 9)}$DASH${substring(9, length)}"

fun String.fillStringWithChar(charToFill: Char?, maxLength: Int): String {
    var term = this
    return if (term.length >= maxLength) term else {
        term = charToFill!! + term
        term.fillStringWithChar(charToFill, maxLength)
    }
}

fun String.capitalizeEveryWord(): String {
    val text = this.lowercase(Locale.ROOT).split(" ")
    var formattedText = ""
    text.forEach { char ->
        formattedText += "${char.replaceFirstChar {
        if (it.isLowerCase()) it.titlecase(
            getDefault()
        ) else it.toString()
    }} " }
    return formattedText.trim()
}

fun String.formatWithMask(mask: String): String {
    if (this.contains("[(]|[)]|\\s|[-]".toRegex())) return this

    var aMask = mask
    for (i in indices) aMask =
        aMask.replaceFirst("#".toRegex(), this.substring(i, i + 1))
    return aMask.replace("#".toRegex(), "")
}