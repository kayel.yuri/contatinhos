package ext

import android.os.Bundle
import androidx.fragment.app.Fragment
import ext_context.isAccessibilityEnabled

fun <T : Fragment> T.newInstance(bundle: Bundle? = null): T {
    arguments = bundle
    return this
}

fun <T : Fragment> T.newInstance(bundleBuilder: Bundle.() -> Unit) = apply {
    Bundle().let {
        bundleBuilder.invoke(it)
        arguments = it
    }
}

fun Fragment.requireAct(block: Fragment.() -> Unit) =
    requireActivity().run { block.invoke(this@requireAct) }

fun Fragment.isAccessibilityEnabled() = context?.isAccessibilityEnabled()

//fun <T> Fragment.observe(liveData: LiveData<T>, observe: T.() -> Unit = {}): T? {
//    liveData.observe(this, observe)
//    return liveData.value
//}
